#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <stdio.h>

#include "persona_libreria.h"

//El diseño de todas las funciones se encuentra en 'persona_libreria.h'

void destruir_Persona (void* dato) {
    if (dato) {
        Persona* persona = (Persona*)dato;

        free(persona->nombre);
        free(persona->lugarDeNacimiento);
        free(persona);
    }
}

Persona* crear_Persona (char* nombre, int edad, char* pais) {
    Persona* personaNueva;
    personaNueva = malloc(sizeof(Persona));

    personaNueva->nombre = nombre;
    personaNueva->edad = edad;
    personaNueva->lugarDeNacimiento = pais;

    return personaNueva;
}

void* copiar_persona(void* dato) {


    Persona* copiaPersona = NULL;

    if (dato) {
        int largoNombre, largoPais;
        Persona* persona = (Persona*) dato;

        //calcula los largos
        largoNombre = strlen(persona->nombre);
        largoPais = strlen(persona->lugarDeNacimiento);

        //asigna los largos
        copiaPersona = malloc(sizeof(Persona));
        copiaPersona->nombre = malloc(sizeof(char) * (largoNombre + 1));
        copiaPersona->lugarDeNacimiento = malloc(sizeof(char) * (largoPais + 1));

        strcpy(copiaPersona->nombre, persona->nombre);
        strcpy(copiaPersona->lugarDeNacimiento, persona->lugarDeNacimiento);
        copiaPersona->edad = persona->edad;
    }
    return copiaPersona;
}

void imprimir_persona (void* dato, FILE* archivo) {
    if (dato) {
        Persona* persona = (Persona*) dato;
        fprintf(archivo, "%s,%d,%s\n",  persona->nombre, persona->edad, persona->lugarDeNacimiento);
    }else{
        fprintf(archivo, "\n");
    }
}

void* poner_nombre_mayuscula (void* dato) {
    Persona* persona = NULL;
    if (dato) {
        persona = (Persona*) dato;
        for(int i = 0; persona->nombre[i]; i++)
            persona->nombre[i] = toupper(persona->nombre[i]);
    }
    return persona;
}

void* avanzar_un_year (void* dato) {
    Persona* persona = NULL;
    if (dato) {
        Persona* persona = (Persona*) dato;
        persona->edad += 1;
        return persona;
    }

    return persona;
}

void* edad_al_reves (void* dato) {
    Persona* persona = NULL;

    if (dato) {
        persona = (Persona*) dato;

        if (persona->edad > 10){

            char edadAux[3], edadAux2[3];

            sprintf(edadAux, "%d", (persona->edad));
            int i = 0;

            for (int j = 1; i < 2; i ++) {
                edadAux2[i] = edadAux[j];
                j--;
            }
            edadAux2[i] = 0;

            persona->edad = atoi(edadAux2);
        }
    }

    return persona;
}

void* ahora_viven_en_rosario (void* dato) {
    Persona* persona = NULL;
    if (dato) {
        persona = (Persona*) dato;
        persona->lugarDeNacimiento = (char*) realloc(persona->lugarDeNacimiento, sizeof(char)*8);
        strcpy(persona->lugarDeNacimiento, "Rosario");
    }
        return persona;
}


int es_mayor (void* dato) {
    if (dato) {
    Persona* persona = (Persona*) dato;
    return (persona->edad >= 18);
    }

    return 0;
}

int empieza_con_a (void* dato) {
    if (dato) {
    Persona* persona = (Persona*) dato;
    return (persona->nombre[0] == 'a' || persona->nombre[0] == 'A');
    }

    return 0;
}

int vive_en_Argentina (void* dato) {
    if (dato) {
        Persona* persona = (Persona*) dato;
        int largoPais, vive = 1;
        char Argentina[10] = "Argentina";
        largoPais = strlen(persona->lugarDeNacimiento);
        if (largoPais != strlen(Argentina)) {
            vive = 0;
            return vive;
        }
        for (int i = 0; i < largoPais && vive; i++) {
            if (persona->lugarDeNacimiento[i] != Argentina[i])
                vive = 0;
        }
        return vive;
    }
    return 0;
}

int contiene_todas_las_vocales_en_nombre (void* dato) {
    if (dato) {
        Persona* persona = (Persona*) dato;
        int vocales[5] = {0,0,0,0,0}, largoNombre;
        largoNombre = strlen (persona->nombre);
        if (largoNombre < 4) {
            return 0;
        }
        for (int i = 0; i < largoNombre; i++) {
            if (persona->nombre[i] == 'a' || persona->nombre[i] == 'A')
                vocales[0] = 1;
            if (persona->nombre[i] == 'e' || persona->nombre[i] == 'E')
                vocales[1] = 1;
            if (persona->nombre[i] == 'i' || persona->nombre[i] == 'I')
                vocales[2] = 1;
            if (persona->nombre[i] == 'o' || persona->nombre[i] == 'O')
                vocales[3] = 1;
            if (persona->nombre[i] == 'u' || persona->nombre[i] == 'U')
                vocales[4] = 1;
        }

        return (vocales[0] && vocales[1] && vocales[2] && vocales[3] && vocales[4]);
    }

    return 0;
}
