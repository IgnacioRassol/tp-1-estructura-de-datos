#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <assert.h>
#include <wchar.h>


///////////////// AUXILIARES

int aleatorio (int min, int max) {

    int numero;
    numero = rand() % (max-min) + min;
    assert(numero >= min && numero <= max);

    return numero;
}

void generar_datos_aleatorios (int cantidadDeNombres, int cantidadDePaises, int datos[]) {
    int nombreAleatorio, paisAleatorio, edadAleatoria;
    nombreAleatorio = aleatorio(0, cantidadDeNombres);
    edadAleatoria = aleatorio(0,100);
    paisAleatorio = aleatorio(0, cantidadDePaises);
    datos[0] = nombreAleatorio;
    datos[1] = edadAleatoria;
    datos[2] = paisAleatorio;
}

void liberar_memoria (wchar_t** matriz, int cantidad) {
    for (int i = 0; i < cantidad; i++)
        free(matriz[i]);
    free(matriz);
}

/////////////////


///////////////// VALIDACION

int validar_entrada (char* rutaArchivo) {
    int leyo;
    wchar_t aux;
    FILE* archivo;
    archivo = fopen(rutaArchivo,"r+");
    if (archivo != NULL) {
        return 1;
    }
    return 0;
}

/////////////////


///////////////// ENTRADA Y SALIDA

int lectura (wchar_t** matriz, char rutaArchivo[]) {
    int i = 0, largo;
    wchar_t* lectura = malloc(sizeof(wchar_t));
    if (validar_entrada(rutaArchivo)) {
        FILE* archivo;
        archivo = fopen(rutaArchivo, "r+");
        assert(archivo != NULL);
        while (lectura) {
            printf("%d\n", i);
            matriz[i] = malloc(sizeof(wchar_t) * 50);
            lectura = fgetws (matriz[i], 50, archivo);
            printf("%p\n", lectura);
            //largo = wcslen(matriz[i])-2;
            //matriz[i][largo] = '\0';
            i++;

        }
        fclose(archivo);
        printf("\nEl archivo fue leido exitosamente.\n");
        return i;
    }
    printf("\nEl archivo ingresa no existe o no se pudo abrir.\n\n");
    return 0;
}

void generar_archivo (int cantidadDeNombres, int cantidadDePaises, char archivoSalida[], wchar_t** nombres, wchar_t** paises) {
    int datos[3], i = 0, nombre, pais, edad, cantidadDePersonas;
    FILE* salida;
    srand (time(NULL));
    cantidadDePersonas = aleatorio(2000, 3000);
    salida = fopen(archivoSalida,"w"); 
    for (; i < cantidadDePersonas; i++) {
        generar_datos_aleatorios(cantidadDeNombres, cantidadDePaises, datos);
        nombre = datos[0];
        edad = datos[1];
        pais = datos[2];
        fprintf(salida,"%ls,%d,%ls\n",nombres[nombre], edad, paises[pais]);
    }
    fclose(salida);
}

/////////////////


///////////////// MENU

void interfaz (wchar_t** nombres, wchar_t** paises) {
    int  cantidadDeNombres = 0, cantidadDePaises = 0, cantidadDePersonas;
    char rutaArchivoNombres[50], rutaArchivoPaises[50], nombreArchivoSalida[50];
    FILE* archivo;
    while (!cantidadDeNombres) {
        printf("Ingrese la ruta del archivo de nombres: (Si esta en el mismo directoria que el ejecutable, solo ingrese el archivo con su extension)\n\n");
        scanf("%s",rutaArchivoNombres);
        cantidadDeNombres = lectura(nombres, rutaArchivoNombres);
    }
    while (!cantidadDePaises) {
        printf("\nIngrese la ruta del archivo de paises: (Si esta en el mismo directoria que el ejecutable, solo ingrese el archivo con su extension)\n\n");
        scanf("%s",rutaArchivoPaises);
        cantidadDePaises = lectura(paises, rutaArchivoPaises);
    }
    printf("\nIngrese el nombre del archivo de salida con su extension\n\n");
    scanf("%s",nombreArchivoSalida);
    generar_archivo(cantidadDeNombres, cantidadDePaises, nombreArchivoSalida, nombres, paises);
    liberar_memoria(nombres, cantidadDeNombres);
    liberar_memoria(paises, cantidadDePaises);
}

/////////////////


///////////////// PRINCIPAL

int main () {
    wchar_t** nombres, **paises;

    nombres = malloc(sizeof(wchar_t*) * 2900);
    paises = malloc(sizeof(wchar_t*) * 2000);

    interfaz(nombres,paises);

    return 0;
}

/////////////////
