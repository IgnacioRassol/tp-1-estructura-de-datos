#include <stdlib.h>

#include "GList.h"

//El diseño de todas las funciones se encuentra en 'GList.h'

int GList_vacio(GList inicio) {
    return (inicio == NULL);
}

GList GList_crear() {
    return NULL;
}

GList GList_crear_header() {
    GList nuevoHeader;
    nuevoHeader = malloc(sizeof(GHeader));
    nuevoHeader->principio = NULL;
    nuevoHeader->fin = NULL;

    return nuevoHeader;
}

GList GList_agregar_inicio (GList inicio, void* dato) {
    GNodoP nuevoNodo;
    nuevoNodo = malloc(sizeof(GNodo));
    nuevoNodo->dato = dato;

    if (GList_vacio(inicio)) {
        GList nuevaLista = GList_crear_header();
        nuevaLista->principio = nuevoNodo;
        nuevaLista->fin = nuevoNodo;
        nuevoNodo->sig = NULL;

        return nuevaLista;
    }

    nuevoNodo->sig = inicio->principio;
    inicio->principio = nuevoNodo;

    return inicio;

}

GList GList_agregar_final (GList inicio, void* dato) {
    GNodoP nuevoNodo;

    nuevoNodo = malloc(sizeof(GNodo));

    nuevoNodo->dato = dato;
    nuevoNodo->sig = NULL;

    if (GList_vacio(inicio)) {
        GList nuevaLista = GList_crear_header();
        nuevaLista->principio = nuevoNodo;
        nuevaLista->fin = nuevoNodo;
        return nuevaLista;
    }

    GNodoP ultimoNodo = inicio->fin;
    ultimoNodo->sig = nuevoNodo;
    inicio->fin = nuevoNodo;
    return inicio;

}

void GList_destruir (GList inicio, Destruir eliminar_dato) {

    if (inicio) {
        GNodoP nodo, auxiliar;
        nodo = inicio->principio;

        for(;nodo;){
            eliminar_dato(nodo->dato);
            auxiliar = nodo;
            nodo = nodo->sig;
            free(auxiliar);
        }

        free(inicio);
    }
}

void GList_imprimir (GList inicio, char* nombreArchivo, Imprimir imp) {
    GNodoP nodo;
    FILE* archivo;
    archivo = fopen(nombreArchivo,"w");

    nodo = inicio->principio;
    for (; nodo;) {
        imp(nodo->dato, archivo);
        nodo = nodo->sig;
    }

    fclose(archivo);
}

GList map (GList lista, Funcion f, Copia c) {
    GNodoP nodo;
    GList listaMapeada;
    void* copiaDato;

    listaMapeada = GList_crear();
    nodo = lista->principio;

    while (nodo) {
        copiaDato = c(nodo->dato);
        copiaDato = f(copiaDato);
        listaMapeada = GList_agregar_final(listaMapeada, copiaDato);
        nodo = nodo->sig;
    }

    return listaMapeada;
}

GList filter (GList lista, Predicado p, Copia c){
    GNodoP nodo;
    GList listaFiltrada;
    void* copiaDato;

    listaFiltrada = GList_crear();
    nodo = lista->principio;

    while (nodo) {
        if (p(nodo->dato)) {
            copiaDato = c(nodo->dato);
            listaFiltrada = GList_agregar_final(listaFiltrada, copiaDato);
        }
        nodo = nodo->sig;
    }

    return listaFiltrada;
}
