# Ignacio Rassol y Facundo Pereyra - TP 1 - Estructura de Datos y Algoritmos I
## Archivos
Nuestro trabajo práctico consta de 6 archivos. "tpParte1" el cual, mediante una interfaz, se encarga de generar los datos de prueba; "tpParte2" el cual se encarga de procesar los datos generados por el archivo anterior y brinda un menu para las ejecuciones de filter y map. Luego "GList.c" en donde se encuentra el codigo de todas las funciones relacionas a nuestra lista generica, el cual incluye el header "GList.h" en donde aparecen las signaturas de nuestras funciones y los typedef. Analogamente, tenemos "persona_libreria.c" en donde se encuentra el codigo de todas las funciones relacionadas a nuestra estructura Persona, que incluye el header "persona_libreria.h" en donde aparecen las signaturas de nuestra funciones y los typedef. 

Estos 6 archivos se deben encontrar en la misma carpeta para que el programa funcione correctamente.

Adicionalmente, se incluyen dos archivos de entrada para la primera parte brindados por la catedra.
#
## Especificaciones

### __Nuestra lista generica__:

La estructura que elegimos utilizar para este trabajo practico es una lista simplemente enlazada,
con la particularidad de que su primer elemento es un "header", el cual contiene dos punteros,
uno al primer nodo de la lista, y otro al ultimo elemento. Los nodos estan definidos de la
misma forma que sugiere el enunciado del trabajo practico. La idea detras de esta implementacion
es facilitar agregar un nuevo elemento al final de la lista, sin traer mas complicaciones 
que surgirian de usar algun otro tipo de estructura.

### __Observaciones__:
-Problema codificacion de archivos:
	Linux: En linux notamos que si trabajabamos con el tipo de dato "char" para caracteres no teniamos problemas excepto a la hora de intentar operar con ellos (por ejemplo, al intentar aplicarle toupper a una letra con tilde). Para solucionar esto intentamos trabajar con el tipo de dato "wchar_t", pero tuvimos problemas a la hora de intentar leer el archivo en la primera parte del trabajo practico (no llegamos a intentar hacer el cambio en la segunda parte, pero suponemos que hubiesemos tenido problemas similares). Dichos problemas fueron mostrados a miembros de la catedra, que coincidieron en que no era un problema simple de resolver. Teniendo en cuenta esto, y que consideramos que resolver eso no era el enfoque principal del trabajo, decidimos dedicarle tiempo a otras areas y dejar eso usando el tipo de dato "char".

-Sobre el uso de "assert":
    Por preferencia personal, decidimos no hacer tanto uso de los "assert", como sugiere la catedra, y reemplazarlo en cambio con validaciones que no interrumpan la ejecucion, permitiendo en lo posible que el programa continue.

### __Flujo del programa__:

Parte 1) Se debe ejecutar "tpParte1":
Dentro de este programa, se le va a pedir primero al usuario que ingrese la ruta del archivo con los nombres, este proceso se va a repetir hasta que la ruta ingresada sea valida. Luego, ocurre lo mismo con el archivo de paises.
Finalmente, se le pide al usuario que ingrese el nombre del archivo de salida con su respectiva extension, el cual se va a generar en la misma carpeta donde se esta ejecutando el programa.

Parte 2) Se deben ejecutar "tpParte2", "GList.c" y "persona_libreria.c":
Dentro de este programa, se le va a pedir al usuario que ingrese la ruta del archivo con la salida del programa anterior ("tpParte1") hasta que ingrese una ruta valida. Luego, el programa brinda un menu con las siguientes opciones:
-0 Finalizar el programa
-1 Aplicar map
-2 Aplicar filter
Si se elege aplicar map o filter, se va a poder elegir entre 4 funciones o predicados en donde cada uno genera un archivo de salida distinto. La ejecucion termina cuando se elige la opcion 0 en el menu

#
## Entrada

Parte 1) Los archivos de entrada para "tpParte1" deben tener la forma de lista de palabras, cada una con un salto de linea al final.

Parte 2) El archivo de entrada para "tpParte2" debe tener la forma:
"
string,int,string\n
string,int,string\n
		.
		.
		.
"
Es decir, una lista en donde en cada fila se encuentran los datos de una persona, separados por coma y al final un salto de linea(notese que consideramos que las strings pueden incluir espacios)


#
## Salida

Parte 1) La salida de "tpParte1" va a tener la siguiente forma:
"
string,int,string\n
string,int,string\n
		.
		.
		.
"
Parte 2) La salida de "tpParte2" siempre va a ser un archivo de la forma
"
string,int,string\n
string,int,string\n
		.
		.
		.
"
con extension ".txt". Que va a corresponder a la lista reciba como parametro luego de haberlo aplicado la funcion map o la funcion filter

