#ifndef _persona_libreria_h
#define _persona_libreria_h

#include <stdio.h>

typedef struct {
    char* nombre;
    int edad;
    char* lugarDeNacimiento; // pais o capital
} Persona;

/**
Aclaracion: Una persona es una estructura que consiste en los campos:
- nombre, el cual es un string
- edad, la cual es un entero
- lugarDeNacimiento, el cual es un string
Donde tanto "nombre" como "lugarDeNacimiento" se definen dinamicamente.

Por lo tanto, cada vez que nos refiramos a "persona" en el dise�o de abajo, vamos a estar
haciendo referencia a la estructura que describimos.
*/


/**
destruir_Persona: Void*
Recibe como parametro una persona. Se encarga de liberar la memoria asignada para
de los datos de la persona (nombre y lugar de nacimiento) y finalmente libera la memoria
asignada para la persona.
*/
void destruir_Persona(void*);

/**
crear_Persona: Char*, Int, Char* -> Persona*
Recibe como parametro los datos de una persona, es decir, nombre edad y apellido. Crea una estructura
persona nueva, guarda los datos en la estructura y devuelve un puntero a la misma
*/
Persona* crear_Persona(char*, int, char*);

/**
copiar_persona: Void* -> Void*
Recibe como parametro un persona. Crea otra persona y copia los datos de la estructura reciba
como parametro en la nueva estructura. Finalmente retorna un puntero a la nueva estructura
*/
void* copiar_persona(void*);

/**
imprimir_persona: void*, FILE*
Recibe como parametro una persona y un puntero a archivo. Se encargar de escribir en el archivo
los datos de la persona de la forma "nombre,edad,lugarDeNacimiento".
*/
void imprimir_persona(void*, FILE*);


/**
poner_nombre_mayuscula: void* -> void*
Recibe como parametro una persona. Devuelve la misma persona solo que ahora las letras de su
nombre estan en mayuscula.
*/
void* poner_nombre_mayuscula(void*);

/**
avanzar_un_year: void* -> void*
Recibe como parametro una persona. Devuelve la misma persona solo que ahora tiene un a�o mas
*/
void* avanzar_un_year (void*);

/**
viven_en_rosario: void* -> void*
Recibe como parametro una persona. Devuelve la misma persona solo que ahora en lugarDeNacimiento
guarda "Rosario".
*/
void* ahora_viven_en_rosario (void*);

/**
edad_al_reves: void* -> void*
Recibe como parametro una persona. Devuelve la misma persona solo que ahora los numeros de su edad
estan invertidos.
*/
void* edad_al_reves (void*);

/**
es_mayor: void* -> int
Recibe como parametro una persona. Devuelve uno si es mayor de edad, en caso contrario,
devuelve cero.
*/
int es_mayor(void*);

/**
empieza_con_a: void* -> int
Recibe como parametro una persona. Devuelve uno si su nombre empieza con la letra a (tanto mayuscula
como minuscula) en caso contrario, devuelve cero.
*/
int empieza_con_a(void*);

/**
vive_en_Argentina: void* -> int
Recibe como parametro una persona. Devuelve uno si su lugarDeNacimineto es Argentina, en caso contrario,
devuelve cero.
*/
int vive_en_Argentina(void*);

/**
contiene_todas_las_vocales_en_nombre: void* -> int
Recibe como parametro una persona. Devuelve uno si en su nombre aparecen todas las vocales, en caso contrario,
devuelve cero.
*/
int contiene_todas_las_vocales_en_nombre(void*);


#endif
