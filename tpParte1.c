///////////////// LIBRERIAS

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <assert.h>

/////////////////


///////////////// CONSTANTES

#define MIN_LECTURA 512
#define INCREMENTO 2

/////////////////


///////////////// SIGNATURAS

/**
aleatorio: Int, Int -> Int
Recibe como parametros dos enteros que representan los extremos de un intervalo
y retorna un numero entero aleatorio que pertenezca al intervalo
*/
int aleatorio (int min, int max);

/**
generar_datos_aleatorios: Int, Int, Int*
Dados la cantidad de nombres y la cantidad de paises, guarda en datos una posicion aleatoria de
la matriz de nombres, una edad aleatoria entre 0 y 100 y una posicion aleatoria de la matriz
de paises
*/
void generar_datos_aleatorios (int cantidadDeNombres, int cantidadDePaises, int datos[]);

/**
liberar_memoria: Char**, Int
Recibe como parametro una matriz de punteros y una cantidad. Libera la memoria de los punteros
que se encuentran en la matriz. Finalmente, libera la matriz.
*/
void liberar_memoria (char** matriz, int cantidad);

/**
validar_entrada: Char* -> Int
Recibe como parametro el nombre del archivo a leer, cheeckea que el archivo
exista y que no este vacio. Si estas 2 condiciones se cumplen retorna 1.
Caso contrario, retorna 0.
*/
int validar_entrada (char* rutaArchivo);

/**
validar_letra: char -> int
Recibe como parametro un char y devuelve 1 en caso que no sea un asterisco.
Si es un asterisco, devuelve 0.
*/
int validar_letra (char letra);

/**
validar_entrada: Char* -> Char*
Recibe como parametro 2 cadenas de caracteres, una vacia y la otra con contenido.
Se encarga de copiar, caracter a caracter la cadena no vacia en la vacia hasta
que encuentra un '\n' o un '\r'
*/
void lectura_aux(char* buffer, char* salida);

/**
lectura: Char***, Char*, int, int -> Int
Recibe como parametro un puntero a una matriz de 2 dimensiones, un nombre de archivo,
y dos enteros que usa para el manejo de memoria. Lee el archivo, guarda las lineas 
en la matriz y retorna la cantidad de filas leidas
*/
int lectura (char*** matriz, char* rutaArchivo, int minimoLectura, int incremento);

/**
generar_archivo: Int, Int, Char*, Char**, Char**
Recibe como parametro la cantidad de nombres y de paises disponibles, el nombre del archivo de salida y los nombres y paises.
Genera una cantidad de personas de forma random, elige nombre, pais y edad aleatoriamente para cada una y escribe una
personas por renglon(de la forma "Nombre,Edad,Pais") en el archivo de salida.
*/
void generar_archivo (int cantidadDeNombres, int cantidadDePaises, char* archivoSalida, char** nombres, char** paises);

/**
interfaz: Char**, Char**, int, int
Tiene como objetivo guiar al usuario para que el programa reciba los archivos que necesita y asi poder escribir el archivo
con las personas. Ademas se encarga del manejo de la memoria para la lectura
*/
void interfaz (char*** nombres, char*** paises, int minimoLectura, int incremento);

/////////////////


///////////////// AUXILIARES

int aleatorio (int min, int max) {
    int numero;
    numero = rand() % (max-min) + min;
    assert (numero >= min && numero <= max);


    return numero;
}

void generar_datos_aleatorios (int cantidadDeNombres, int cantidadDePaises, int datos[]) {
    int nombreAleatorio, paisAleatorio, edadAleatoria;
    nombreAleatorio = aleatorio(0, cantidadDeNombres);
    edadAleatoria = aleatorio(0,99);
    paisAleatorio = aleatorio(0, cantidadDePaises);
    datos[0] = nombreAleatorio;
    datos[1] = edadAleatoria;
    datos[2] = paisAleatorio;
}

void liberar_memoria (char** matriz, int cantidad) {
    for (int i = 0; i < cantidad; i++)
        free(matriz[i]);
    free(matriz);
}

/////////////////


///////////////// VALIDACION

int validar_entrada (char* rutaArchivo) {
    char aux;
    FILE* archivo;
    archivo = fopen(rutaArchivo,"r");
    if (archivo != NULL) {
        aux = fgetc (archivo);
        if (aux != EOF) {
            fclose(archivo);
            return 1;
        }
    }
    return 0;
}

//revisa que no sea un asterisco
int validar_letra (char letra){
    if (letra != 42)
        return 1;

    return 0;
}

/////////////////


///////////////// ENTRADA Y SALIDA

void lectura_aux(char* buffer, char* salida) {
    int i, j;

    for (i = 0, j = 0; buffer[i] != '\r' && buffer[i] != '\n'; i++)
        if (validar_letra(buffer[i])) {

            salida[j] = buffer[i];
            j++;
        }

    salida[j] = 0;
}

//Recibe un puntero a matriz de punteros porque necesita reallocarla
int lectura (char*** matriz, char* rutaArchivo, int minimoLectura, int incremento) {
    int i = 0, k, largo, cantidadMax = minimoLectura;
    char** auxiliar, buffer[50];
    auxiliar = *matriz;

    if (validar_entrada(rutaArchivo)) {
        FILE* archivo;
        archivo = fopen(rutaArchivo, "r");


        while (!feof(archivo)) {

// cuando se llega a la cantidad maxima, se realoca la memoria, con la nueva memoria siendo 
// el doble que la anterior

            if (i == (cantidadMax)) {
                cantidadMax = cantidadMax * incremento;
                auxiliar = (char**) realloc(auxiliar, cantidadMax * (sizeof (char*)));
            }

            auxiliar[i] = malloc(50 * sizeof(char));
            fgets (buffer, 50, archivo);

            lectura_aux(buffer, auxiliar[i]);

            i++;
        }
        fclose(archivo);

        *matriz = auxiliar;
        printf("\nEl archivo fue leido exitosamente.\n");
        return i;
    }
    printf("\nEl archivo ingresado no existe o no se pudo abrir.\n\n");
    return 0;
}

void generar_archivo (int cantidadDeNombres, int cantidadDePaises, char archivoSalida[], char** nombres, char** paises) {

    int datos[3], i = 0, nombre, pais, edad, cantidadDePersonas;
    FILE* salida;
    srand (time(NULL));
    cantidadDePersonas = aleatorio(2000, 3000);
    salida = fopen(archivoSalida,"w");

    for (; i < cantidadDePersonas-1; i++) {
        generar_datos_aleatorios(cantidadDeNombres, cantidadDePaises, datos);
        nombre = datos[0];
        edad = datos[1];
        pais = datos[2];
        fprintf(salida,"%s,%d,%s\n",nombres[nombre], edad, paises[pais]);
    }
    //Hace la ultima iteracion afuera del for para printear sin el \n, para facilitar la lectura del archivo
    // de salida
    generar_datos_aleatorios(cantidadDeNombres, cantidadDePaises, datos);
    nombre = datos[0];
    edad = datos[1];
    pais = datos[2];
    fprintf(salida,"%s,%d,%s",nombres[nombre], edad, paises[pais]);

    fclose(salida);
}

/////////////////


///////////////// INTERFAZ

void interfaz (char*** nombres, char*** paises, int minimoLectura, int incremento) {
    int  cantidadDeNombres = 0, cantidadDePaises = 0, cantidadDePersonas;
    char rutaArchivoNombres[50], rutaArchivoPaises[50],nombreArchivoSalida[50];
    FILE* archivo;
    while (!cantidadDeNombres) {
        printf("Ingrese la ruta del archivo de nombres: (Si esta en el mismo directoria que el ejecutable, solo ingrese el archivo con su extension)\n\n");
        scanf("%s",rutaArchivoNombres);
        cantidadDeNombres = lectura(nombres, rutaArchivoNombres, minimoLectura, incremento);
    }
    while (!cantidadDePaises) {
        printf("\nIngrese la ruta del archivo de paises: (Si esta en el mismo directoria que el ejecutable, solo ingrese el archivo con su extension)\n\n");
        scanf("%s",rutaArchivoPaises);
        cantidadDePaises = lectura(paises, rutaArchivoPaises, minimoLectura, incremento);
    }
    printf("\nIngrese el nombre del archivo de salida con su extension\n\n");
    scanf("%s",nombreArchivoSalida);
    generar_archivo(cantidadDeNombres, cantidadDePaises, nombreArchivoSalida, *nombres, *paises);

    liberar_memoria(*nombres, cantidadDeNombres);
    liberar_memoria(*paises, cantidadDePaises);
}

/////////////////


///////////////// PRINCIPAL

int main () {
    char** nombres, **paises;

    nombres = malloc(sizeof(char*) * MIN_LECTURA);
    paises = malloc(sizeof(char*) * MIN_LECTURA);

    interfaz(&nombres,&paises, MIN_LECTURA, INCREMENTO);

    return 0;
}

/////////////////

